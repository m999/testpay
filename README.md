## Приложение TestPay sandbox имитирует платежную систему TestPay.

## Информация для использования программы: 
1. Для сборки используется Gradle v4.4+ и Java 8. 
2. Перед запуском программы необходимо указать client-id, client-secret, scope и secret-word в src/main/resources/config.properties. 