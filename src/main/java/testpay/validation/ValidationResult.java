package testpay.validation;

import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import testpay.exception.ValidationException;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ValidationResult {

    private List<String> errors = new ArrayList<>();

    public ValidationResult add(String error) {
        errors.add(error);
        return this;
    }

    public ValidationResult add(ValidationResult result) {
        errors.addAll(result.getErrors());
        return this;
    }

    public void throwIfHasError() {
        if (CollectionUtils.isNotEmpty(errors)) {
            throw new ValidationException(String.join(". ", errors));
        }
    }

    public boolean hasErrors() {
        return CollectionUtils.isNotEmpty(errors);
    }
}
