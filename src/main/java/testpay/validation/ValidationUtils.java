package testpay.validation;

import org.apache.commons.lang3.StringUtils;

public class ValidationUtils {

    public static ValidationResult validateNull(Object o, String msg) {
        ValidationResult result = new ValidationResult();
        if (null == o) {
            result.add(msg);
        }
        return result;
    }

    public static ValidationResult validateEmpty(String s, String msg) {
        ValidationResult result = new ValidationResult();
        if (StringUtils.isBlank(s)) {
            result.add(msg);
        }
        return result;
    }

    public static ValidationResult validateTrue(boolean b, String msg) {
        ValidationResult result = new ValidationResult();
        if (b) {
            result.add(msg);
        }
        return result;
    }

    public static ValidationResult validateFalse(boolean b, String msg) {
        ValidationResult result = new ValidationResult();
        if (!b) {
            result.add(msg);
        }
        return result;
    }
}
