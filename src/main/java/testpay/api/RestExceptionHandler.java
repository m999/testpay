package testpay.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import testpay.exception.ApplicationException;
import testpay.exception.ValidationException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handle validation exception
     *
     * @param ex an exception thrown during validating entities
     * @return HttpStatus 400 Bad Request with detailed error information
     */
    @ExceptionHandler(value = {ValidationException.class})
    protected ResponseEntity handleValidationException(ValidationException ex) {
        ErrorMessage errorMessage = new ErrorMessage(OAuth2Exception.INVALID_REQUEST, ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
    }

    /**
     * Handle application exception
     *
     * @param ex an exception at application run time
     * @return HttpStatus 500 Internal Server Error with detailed error information
     */
    @ExceptionHandler(value = {ApplicationException.class})
    protected ResponseEntity handleApplicationException(ApplicationException ex) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
    }
}
