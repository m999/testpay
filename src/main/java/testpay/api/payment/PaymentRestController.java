package testpay.api.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import testpay.domain.payment.Payment;
import testpay.domain.payment.PaymentDTO;
import testpay.domain.payment.PaymentDTOAssembler;
import testpay.service.payment.PaymentService;

@RestController
@RequestMapping("/payments")
public class PaymentRestController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentDTOAssembler paymentDTOAssembler;

    @PostMapping("/payment")
    public ResponseEntity<PaymentDTO> payment(@RequestBody Payment payment) {
        return ResponseEntity.ok().body(paymentDTOAssembler.assemble(paymentService.createPayment(
                payment.getIntent(), payment.getNotification_url(), payment.getPayer(), payment.getTransaction())));
    }
}
