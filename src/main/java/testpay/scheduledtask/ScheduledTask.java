package testpay.scheduledtask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import testpay.repository.WebhookRepository;
import testpay.service.webhook.WebhookService;
import testpay.util.DateUtil;

import java.time.LocalDateTime;

@Component
public class ScheduledTask {

    @Autowired
    private WebhookService webhookService;

    @Autowired
    private WebhookRepository webhookRepository;

    /**
     * Check Webhook repository every 2 hours and try to find undelivered webhooks.
     * Webhook should be created not later than 3 days ago and number of tries to send should be less than 25
     */
    @Scheduled(fixedRate = 7200000)
    private void sendUndeliveredWebhook() {
        webhookRepository.findByDeliveredIsFalse()
                .stream()
                .filter(webhook -> webhook.getNumberOfTries() < 25)
                .filter(webhook -> DateUtil.toLocalDateTime(webhook.getCreateTime()).isAfter(LocalDateTime.now().minusDays(3)))
                .forEach(webhookService::tryToSendNotification);
    }
}
