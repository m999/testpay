package testpay.domain.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import testpay.domain.amount.AmountValidator;
import testpay.validation.ValidationResult;
import testpay.validation.ValidationUtils;
import testpay.validation.Validator;

@Component
public class TransactionValidator implements Validator<Transaction> {

    @Autowired
    private AmountValidator amountValidator;

    @Override
    public ValidationResult validate(Transaction transaction) {

        ValidationResult result = new ValidationResult();
        result.add(ValidationUtils.validateNull(transaction.getAmount(), "Transaction amount is required"));

        if (null != transaction.getAmount()) {
            result.add(amountValidator.validate(transaction.getAmount()));
        }
        return result;
    }
}
