package testpay.domain.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import testpay.domain.amount.Amount;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "testpay_transaction")
@Getter
@NoArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uui2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID id;

    @Column(name = "s_external_id", unique = true)
    private String external_id;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "amount_id", foreignKey = @ForeignKey(name = "FK_transaction_amount"))
    private Amount amount;

    @Column(name = "s_description")
    private String description;

    public Transaction(String external_id, Amount amount, String description) {
        this.external_id = external_id;
        this.amount = amount;
        this.description = description;
    }
}
