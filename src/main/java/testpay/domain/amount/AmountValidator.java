package testpay.domain.amount;

import org.springframework.stereotype.Component;
import testpay.validation.ValidationResult;
import testpay.validation.ValidationUtils;
import testpay.validation.Validator;

import java.util.Currency;

@Component
public class AmountValidator implements Validator<Amount> {

    @Override
    public ValidationResult validate(Amount amount) {

        ValidationResult result = new ValidationResult();
        result
                .add(ValidationUtils.validateEmpty(amount.getCurrency(), "Amount currency is required"))
                .add(ValidationUtils.validateEmpty(amount.getValue(), "Amount value is required"))
                .add(ValidationUtils.validateTrue(amount.getValue() != null && amount.getValue().length() > 10,
                        "Amount value maximum length is 10 characters."))
                .add(ValidationUtils.validateTrue(amount.getValue() != null && !amount.getValue().matches("[0-9]+(\\.[0-9][0-9]?)?"),
                        "Amount value supports two decimal places"))
                .add(ValidationUtils.validateTrue(
                        Currency.getAvailableCurrencies()
                                .stream()
                                .map(Currency::getCurrencyCode)
                                .noneMatch(currency -> currency.equals(amount.getCurrency())),
                        "Amount currency not match ISO-4217 currency code"));
        return result;
    }
}
