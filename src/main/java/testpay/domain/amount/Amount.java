package testpay.domain.amount;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "payment_amount")
@Getter
@NoArgsConstructor
public class Amount {

    private static final int LENGTH_VALUE = 10;
    private static final int LENGTH_CURRENCY = 3;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uui2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID id;

    @Column(name = "s_value", nullable = false, length = LENGTH_VALUE)
    private String value;

    @Column(name = "s_currency", nullable = false, length = LENGTH_CURRENCY)
    private String currency;

    public Amount(String value, String currency) {
        this.value = value;
        this.currency = currency;
    }
}
