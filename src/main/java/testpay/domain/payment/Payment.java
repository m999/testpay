package testpay.domain.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import testpay.domain.payer.Payer;
import testpay.domain.transaction.Transaction;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "testpay_payment")
@NoArgsConstructor
@Getter
public class Payment {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uui2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID id;

    @Column(name = "d_create_time", nullable = false)
    private Date createTime;

    @Column(name = "s_state", nullable = false)
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "s_intent", nullable = false)
    @Enumerated(EnumType.STRING)
    private Intent intent;

    @Column(name = "s_notification_url", nullable = false)
    private String notification_url;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "payer_id", nullable = false, foreignKey = @ForeignKey(name = "FK_payment_payer"))
    private Payer payer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "transaction_id", nullable = false, foreignKey = @ForeignKey(name = "FK_payment_transaction"))
    private Transaction transaction;

    public Payment(Intent intent, String notificationUrl, Payer payer, Transaction transaction) {
        this.createTime = new Date();
        this.state = State.CREATED;
        this.intent = intent;
        this.notification_url = notificationUrl;
        this.payer = payer;
        this.transaction = transaction;
    }
}
