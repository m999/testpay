package testpay.domain.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum State {

    CREATED("created"),
    APPROVED("approved"),
    FAILED("failed");

    private String description;
}
