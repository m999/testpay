package testpay.domain.payment;

import org.springframework.stereotype.Component;
import testpay.util.DateUtil;

@Component
public class PaymentDTOAssembler {

    public PaymentDTO assemble(Payment payment) {
        PaymentDTO dto = new PaymentDTO();
        dto.setId(payment.getId());
        dto.setCreate_time(payment.getCreateTime());
        dto.setState(payment.getState().getDescription());
        return dto;
    }
}
