package testpay.domain.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Intent {

    @JsonProperty("order")
    ORDER("order"),
    @JsonProperty("cancel")
    CANCEL("cancel");

    private String description;
}
