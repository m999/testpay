package testpay.domain.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import testpay.domain.payer.PayerValidator;
import testpay.domain.transaction.TransactionValidator;
import testpay.validation.ValidationResult;
import testpay.validation.ValidationUtils;
import testpay.validation.Validator;

@Component
public class PaymentValidator implements Validator<Payment> {

    @Autowired
    private PayerValidator payerValidator;

    @Autowired
    private TransactionValidator transactionValidator;

    @Override
    public ValidationResult validate(Payment payment) {

        ValidationResult result = new ValidationResult();
        result
                .add(ValidationUtils.validateNull(payment.getPayer(), "Payment payer is required"))
                .add(ValidationUtils.validateNull(payment.getTransaction(), "Payment transaction is required"))
                .add(ValidationUtils.validateEmpty(payment.getNotification_url(), "Payment notification url is required"))
                .add(ValidationUtils.validateFalse(Intent.ORDER.equals(payment.getIntent()),
                        "Payment intent available value is " + Intent.ORDER.getDescription()));

        if (null != payment.getPayer()) {
            result.add(payerValidator.validate(payment.getPayer()));
        }
        if (null != payment.getTransaction()) {
            result.add(transactionValidator.validate(payment.getTransaction()));
        }

        return result;
    }
}
