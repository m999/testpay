package testpay.domain.payer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "testpay_payer")
@Getter
@NoArgsConstructor
public class Payer {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uui2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID id;

    @Column(name = "s_email", nullable = false)
    private String email;

    public Payer(String email) {
        this.email = email;
    }
}
