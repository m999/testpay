package testpay.domain.payer;

import org.springframework.stereotype.Component;
import testpay.validation.ValidationResult;
import testpay.validation.ValidationUtils;
import testpay.validation.Validator;

@Component
public class PayerValidator implements Validator<Payer> {

    @Override
    public ValidationResult validate(Payer payer) {

        ValidationResult result = new ValidationResult();
        result.add(ValidationUtils.validateEmpty(payer.getEmail(), "Payer email is required"));
        return result;
    }
}
