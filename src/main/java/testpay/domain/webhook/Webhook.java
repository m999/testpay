package testpay.domain.webhook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import testpay.domain.payment.Payment;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "testpay_webhook_notification")
@Getter
@NoArgsConstructor
public class Webhook {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uui2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID id;

    @Column(name = "d_create_time", nullable = false)
    private Date createTime;

    @ManyToOne
    @JoinColumn(name = "payment_id", nullable = false, foreignKey = @ForeignKey(name = "FK_webhook_notification__payment"))
    private Payment payment;

    @Column(name = "i_number_of_tries", nullable = false)
    private int numberOfTries;

    @Column(name = "b_delivered")
    private boolean delivered;

    public Webhook(Payment payment) {
        this.createTime = new Date();
        this.payment = payment;
        this.numberOfTries = 0;
        this.delivered = false;
    }

    public void incrementNumberOfTries() {
        this.numberOfTries += 1;
    }

    public void delivered() {
        this.delivered = true;
    }
}
