package testpay.domain.webhook;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import testpay.exception.ApplicationException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

@Component
public class WebhookDTOAssembler {

    @Value("${webhook.sha2.secret-word}")
    private String secretWord;

    public WebhookDTO assemble(Webhook webhook) {
        WebhookDTO dto = new WebhookDTO();
        dto.setCurrency(webhook.getPayment().getTransaction().getAmount().getCurrency());
        dto.setAmount(webhook.getPayment().getTransaction().getAmount().getValue());
        dto.setId(webhook.getPayment().getId());
        dto.setExternal_id(webhook.getPayment().getTransaction().getExternal_id());
        dto.setStatus(webhook.getPayment().getState().getDescription());
        dto.setSha2sig(DigestUtils.sha256Hex(
                dto.getCurrency()
                        + dto.getAmount()
                        + DigestUtils.sha256Hex(secretWord.getBytes(StandardCharsets.US_ASCII)).toUpperCase()
                        + dto.getId()
                        + dto.getExternal_id()
                        + dto.getStatus()));
        return dto;
    }
}
