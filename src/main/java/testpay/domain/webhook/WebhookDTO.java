package testpay.domain.webhook;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class WebhookDTO {

    private String currency;
    private String amount;
    private UUID id;
    private String external_id;
    private String status;
    private String sha2sig;
}
