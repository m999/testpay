package testpay.service.payment;

import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.transaction.Transaction;

import java.util.UUID;

public interface PaymentService {

    Payment createPayment(Intent intent, String notificationUrl, Payer payer, Transaction transaction);

    Payment paymentOrThrow(UUID paymentId);
}
