package testpay.service.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.payment.PaymentValidator;
import testpay.domain.transaction.Transaction;
import testpay.repository.PaymentRepository;
import testpay.service.webhook.WebhookService;

import java.util.Optional;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private WebhookService webhookService;

    @Autowired
    private PaymentValidator paymentValidator;

    @Autowired
    private PaymentRepository paymentRepository;

    /**
     * Firstly try to find existing payment by external_id and return it.
     * Else we create new one, validate it and create webhook.
     *
     * @param intent a payment intent
     * @param notificationUrl a url to send webhook
     * @param payer a payer
     * @param transaction a transaction
     * @return an existing Payment was found by transaction external_id or created.
     */
    @Override
    @Transactional
    public Payment createPayment(Intent intent, String notificationUrl, Payer payer, Transaction transaction) {
        Optional<Payment> stored = paymentRepository.findByTransactionExternalId(transaction.getExternal_id());
        if (stored.isPresent()) {
            return stored.get();
        } else {
            Payment payment = new Payment(intent, notificationUrl, payer, transaction);
            paymentValidator.validate(payment).throwIfHasError();
            paymentRepository.save(payment);
            webhookService.createWebHook(payment);
            return payment;
        }
    }

    @Override
    public Payment paymentOrThrow(UUID paymentId) {
        return paymentRepository.findById(paymentId)
                .orElseThrow(() -> new IllegalArgumentException("Payment entity not found"));
    }
}
