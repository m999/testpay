package testpay.service.webhook;

import testpay.domain.payment.Payment;
import testpay.domain.webhook.Webhook;

public interface WebhookService {

    void createWebHook(Payment payment);

    void tryToSendNotification(Webhook webhook);
}
