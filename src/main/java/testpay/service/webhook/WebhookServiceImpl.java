package testpay.service.webhook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import testpay.domain.payment.Payment;
import testpay.domain.webhook.Webhook;
import testpay.domain.webhook.WebhookDTOAssembler;
import testpay.repository.WebhookRepository;
import testpay.service.rest.RestService;

@Service
public class WebhookServiceImpl implements WebhookService {

    @Autowired
    private RestService restService;

    @Autowired
    private WebhookRepository webhookRepository;

    @Autowired
    private WebhookDTOAssembler webhookDTOAssembler;

    @Override
    @Transactional
    public void createWebHook(Payment payment) {
        Webhook webhook = new Webhook(payment);
        tryToSendNotification(webhook);
        webhookRepository.save(webhook);
    }

    /**
     * Try to send notification.
     * If response has HttpStatus 200 webhook status change to delivered.
     * Else if there is HttpClientException, no response or HttpStatus not equals HttpStatus 200 do nothing.
     *
     * @param webhook the webhook
     */
    @Override
    @Transactional
    public void tryToSendNotification(Webhook webhook) {
        webhook.incrementNumberOfTries();
        try {
            ResponseEntity<String> responseEntity = restService.createPostRequest(
                    webhookDTOAssembler.assemble(webhook),
                    webhook.getPayment().getNotification_url());
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                webhook.delivered();
            }
        } catch (HttpClientErrorException e) {
            // do nothing
        }
        webhookRepository.save(webhook);
    }
}
