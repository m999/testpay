package testpay.service.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestServiceImpl implements RestService {

    @Override
    public ResponseEntity<String> createPostRequest(Object object, String url) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(url, object, String.class);
    }
}
