package testpay.service.rest;

import org.springframework.http.ResponseEntity;

public interface RestService {

    ResponseEntity<String> createPostRequest(Object object, String url);
}
