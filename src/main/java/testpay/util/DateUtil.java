package testpay.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {

    public static LocalDateTime toLocalDateTime(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static String formatISO8601(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .format(date);
    }
}
