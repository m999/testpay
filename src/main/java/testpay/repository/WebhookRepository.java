package testpay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import testpay.domain.payment.Payment;
import testpay.domain.webhook.Webhook;

import java.util.List;
import java.util.UUID;

public interface WebhookRepository extends JpaRepository<Webhook, UUID> {

    List<Webhook> findByDeliveredIsFalse();

    List<Webhook> findByPayment(Payment payment);
}
