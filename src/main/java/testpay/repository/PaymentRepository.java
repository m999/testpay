package testpay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import testpay.domain.payment.Payment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {

    @Query(value = "select p.* from testpay_payment p join testpay_transaction t where t.s_external_id = ?1", nativeQuery = true)
    Optional<Payment> findByTransactionExternalId(String externalId);
}
