package testpay.dto;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.payment.State;
import testpay.domain.transaction.Transaction;
import testpay.domain.webhook.Webhook;
import testpay.domain.webhook.WebhookDTO;
import testpay.domain.webhook.WebhookDTOAssembler;
import testpay.repository.WebhookRepository;
import testpay.service.payment.PaymentService;

import java.nio.charset.StandardCharsets;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class WebhookDTOAssemblerTest {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private WebhookRepository webhookRepository;

    @Autowired
    private WebhookDTOAssembler webhookDTOAssembler;

    @Value("${webhook.sha2.secret-word}")
    private String secretWord;

    private ClientAndServer mockServer;

    @BeforeAll
    void startServer() {
        mockServer = ClientAndServer.startClientAndServer(1090);
    }

    @AfterAll
    void stopServer() {
        mockServer.stop();
    }

    @Test
    void createWebhookDTO_success() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        List<Webhook> webhookList = webhookRepository.findByPayment(payment);

        Assertions.assertEquals(1, webhookList.size());
        Webhook webhook = webhookList.get(0);
        WebhookDTO dto = webhookDTOAssembler.assemble(webhook);

        String sha256 = DigestUtils.sha256Hex(
                dto.getCurrency()
                        + dto.getAmount()
                        + DigestUtils.sha256Hex(secretWord.getBytes(StandardCharsets.US_ASCII)).toUpperCase()
                        + dto.getId()
                        + dto.getExternal_id()
                        + dto.getStatus());

        Assertions.assertEquals("RUB", dto.getCurrency());
        Assertions.assertEquals("5.34", dto.getAmount());
        Assertions.assertEquals(payment.getId(), dto.getId());
        Assertions.assertEquals("1234567890", dto.getExternal_id());
        Assertions.assertEquals(State.CREATED.getDescription(), dto.getStatus());
        Assertions.assertEquals(sha256, dto.getSha2sig());
    }
}
