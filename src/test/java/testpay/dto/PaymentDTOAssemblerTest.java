package testpay.dto;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.payment.PaymentDTO;
import testpay.domain.payment.PaymentDTOAssembler;
import testpay.domain.transaction.Transaction;
import testpay.service.payment.PaymentService;

import java.text.SimpleDateFormat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class PaymentDTOAssemblerTest {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentDTOAssembler paymentDTOAssembler;

    private ClientAndServer mockServer;

    @BeforeAll
    void startServer() {
        mockServer = ClientAndServer.startClientAndServer(1090);
    }

    @AfterAll
    void stopServer() {
        mockServer.stop();
    }

    @Test
    void createPaymentDTO_success() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        PaymentDTO dto = paymentDTOAssembler.assemble(payment);

        Assertions.assertEquals(payment.getId(), dto.getId());
        Assertions.assertEquals(payment.getState().getDescription(), dto.getState());
        Assertions.assertEquals(payment.getCreateTime(), dto.getCreate_time());
    }
}
