package testpay.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.payment.State;
import testpay.domain.transaction.Transaction;
import testpay.repository.PaymentRepository;
import testpay.service.payment.PaymentService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class PaymentServiceTest {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentRepository paymentRepository;

    private ClientAndServer mockServer;

    @BeforeAll
    void startServer() {
        mockServer = ClientAndServer.startClientAndServer(1090);
    }

    @AfterAll
    void stopServer() {
        mockServer.stop();
    }

    @Test
    void createPayment_success() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        Payment stored = paymentService.paymentOrThrow(payment.getId());

        Assertions.assertEquals(payment.getId(), stored.getId());
        Assertions.assertEquals(Intent.ORDER, stored.getIntent());
        Assertions.assertEquals("http://localhost:1090", stored.getNotification_url());
        Assertions.assertEquals("payer@payer.com", stored.getPayer().getEmail());
        Assertions.assertEquals("5.34", stored.getTransaction().getAmount().getValue());
        Assertions.assertEquals("RUB", stored.getTransaction().getAmount().getCurrency());
        Assertions.assertEquals("1234567890", stored.getTransaction().getExternal_id());
        Assertions.assertEquals("description", stored.getTransaction().getDescription());
        Assertions.assertEquals(State.CREATED, stored.getState());
    }

    @Test
    void createPayment_paymentIdempotent() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        Payment payment2 = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        Assertions.assertEquals(payment.getId(), payment2.getId());
        Assertions.assertEquals(1, paymentRepository.findAll().size());
    }
}
