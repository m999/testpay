package testpay.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.transaction.Transaction;
import testpay.domain.webhook.Webhook;
import testpay.repository.WebhookRepository;
import testpay.service.payment.PaymentService;

import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class WebhookServiceTest {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private WebhookRepository webhookRepository;

    private ClientAndServer mockServer;

    @BeforeAll
    void startServer() {
        mockServer = ClientAndServer.startClientAndServer(1090);
    }

    @AfterAll
    void stopServer() {
        mockServer.stop();
    }

    @Test
    void createWebhook_success() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        List<Webhook> webhookList = webhookRepository.findByPayment(payment);

        Assertions.assertEquals(1, webhookList.size());
        Assertions.assertEquals(payment.getId(), webhookList.get(0).getPayment().getId());
        Assertions.assertEquals(1, webhookList.get(0).getNumberOfTries());
        Assertions.assertFalse(webhookList.get(0).isDelivered());
    }

    @Test
    void createWebhook_webhookIdempotent() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "https://www.google.com/", payer, transaction);

        paymentService.createPayment(
                Intent.ORDER, "https://www.google.com/", payer, transaction);

        List<Webhook> webhookList = webhookRepository.findByPayment(payment);

        Assertions.assertEquals(1, webhookList.size());
        Assertions.assertEquals(payment.getId(), webhookList.get(0).getPayment().getId());
        Assertions.assertEquals(1, webhookList.get(0).getNumberOfTries());
        Assertions.assertFalse(webhookList.get(0).isDelivered());
    }

    @Test
    void deliverWebhook() {
        MockServerClient client = new MockServerClient("localhost", 1090)
                .reset();

        client.when(HttpRequest.request().withMethod("POST"))
                .respond(HttpResponse.response().withStatusCode(200));

        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = paymentService.createPayment(
                Intent.ORDER, "http://localhost:1090", payer, transaction);

        List<Webhook> webhookList = webhookRepository.findByPayment(payment);

        Assertions.assertEquals(1, webhookList.size());
        Assertions.assertEquals(payment.getId(), webhookList.get(0).getPayment().getId());
        Assertions.assertEquals(1, webhookList.get(0).getNumberOfTries());
        Assertions.assertTrue(webhookList.get(0).isDelivered());

        client.reset();
    }
}
