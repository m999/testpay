package testpay.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.payer.Payer;
import testpay.domain.payer.PayerValidator;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
public class PayerValidatorTest {

    @Autowired
    private PayerValidator payerValidator;

    @Test
    void validate_success() {
        Payer payer = new Payer("email@email.com");
        ValidationResult result = payerValidator.validate(payer);

        Assertions.assertFalse(result.hasErrors());
    }

    @Test
    void validate_nullEmail() {
        Payer payer = new Payer(null);
        ValidationResult result = payerValidator.validate(payer);

        Assertions.assertEquals("Payer email is required", result.getErrors().get(0));
    }

    @Test
    void validate_emptyEmail() {
        Payer payer = new Payer(" ");
        ValidationResult result = payerValidator.validate(payer);

        Assertions.assertEquals("Payer email is required", result.getErrors().get(0));
    }
}
