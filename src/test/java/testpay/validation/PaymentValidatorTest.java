package testpay.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.payer.Payer;
import testpay.domain.payment.Intent;
import testpay.domain.payment.Payment;
import testpay.domain.payment.PaymentValidator;
import testpay.domain.transaction.Transaction;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class PaymentValidatorTest {

    @Autowired
    private PaymentValidator paymentValidator;

    @Test
    void validate_success() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(Intent.ORDER, "http://localhost:1090", payer, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertFalse(result.hasErrors());
    }

    @Test
    void validate_payerNull() {
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(Intent.ORDER, "http://localhost:1090", null, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment payer is required", result.getErrors().get(0));
    }

    @Test
    void validate_transactionNull() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");

        Payment payment = new Payment(Intent.ORDER, "http://localhost:1090", payer, null);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment transaction is required", result.getErrors().get(0));
    }

    @Test
    void validate_notificationUrlEmpty() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(Intent.ORDER, " ", payer, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment notification url is required", result.getErrors().get(0));
    }

    @Test
    void validate_notificationUrlNull() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(Intent.ORDER, null, payer, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment notification url is required", result.getErrors().get(0));
    }

    @Test
    void validate_intentNull() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(null, "http://localhost:1090", payer, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment intent available value is " + Intent.ORDER.getDescription(), result.getErrors().get(0));
    }

    @Test
    void validate_intentIncorrect() {
        Payer payer = new Payer("payer@payer.com");
        Amount amount = new Amount("5.34", "RUB");
        Transaction transaction = new Transaction("1234567890", amount, "description");

        Payment payment = new Payment(Intent.CANCEL, "http://localhost:1090", payer, transaction);
        ValidationResult result = paymentValidator.validate(payment);

        Assertions.assertEquals("Payment intent available value is " + Intent.ORDER.getDescription(), result.getErrors().get(0));
    }
}
