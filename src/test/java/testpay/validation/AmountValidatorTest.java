package testpay.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import testpay.domain.amount.Amount;
import testpay.domain.amount.AmountValidator;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class AmountValidatorTest {

    @Autowired private AmountValidator amountValidator;

    @Test
    void validate_success() {
        Amount amount = new Amount("5.67", "RUB");
        ValidationResult result = amountValidator.validate(amount);

        Assertions.assertFalse(result.hasErrors());
    }

    @Test
    void validate_incorrectValue() {
        Amount amount = new Amount("5.672", "RUB");
        ValidationResult result = amountValidator.validate(amount);

        Assertions.assertEquals("Amount value supports two decimal places", result.getErrors().get(0));
    }

    @Test
    void validate_incorrectCurrency() {
        Amount amount = new Amount("5.67", "dollar");
        ValidationResult result = amountValidator.validate(amount);

        Assertions.assertEquals("Amount currency not match ISO-4217 currency code", result.getErrors().get(0));
    }

    @Test
    void validate_incorrectLength() {
        Amount amount = new Amount("12364325.67", "dollar");
        ValidationResult result = amountValidator.validate(amount);

        Assertions.assertEquals("Amount value maximum length is 10 characters.", result.getErrors().get(0));
    }
}
